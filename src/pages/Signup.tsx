import React, { FC, useCallback, } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { TextInput, PasswordInput, Button, Center, Space } from '@mantine/core';
import { useForm } from '@mantine/hooks';

import { registerRequest } from '../redux/slices/user/actions'
import { selectUserState } from '../redux/slices/user/selectors'

import { RegisterRequestData } from '../services/types';
import { Link } from 'react-router-dom';

const Signup: FC = () => {
  const form = useForm<RegisterRequestData>({
    initialValues: {
      username: '',
      email: '',
      password: '',
    },
    validationRules: {
      email: (value) => /^\S+@\S+$/.test(value),
      username: (value) => /[a-zA-Z0-9._-]{3,20}/.test(value),
      password: (value) => /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,}$/.test(value)
    },
  });

  const { submit } = useSelector(selectUserState);

  const dispatch = useDispatch();

  const onChange = useCallback((event) => {
    form.setFieldValue(event.target.name, event.target.value);
  }, [form])

  const onSubmit = useCallback((values) => {
    dispatch(registerRequest(values));
  }, [dispatch])

  return (
    <Center style={{ height: '100vh' }}>
      <form onSubmit={form.onSubmit(onSubmit)}>
        <TextInput
          required
          name="username"
          label="User name"
          value={form.values.username}
          error={form.errors.username}
          onChange={onChange}
        />
        <TextInput
          required
          name="email"
          label="Email"
          placeholder="your@email.com"
          value={form.values.email}
          error={form.errors.email}
          onChange={onChange}
        />
        <PasswordInput
          required
          name="password"
          label="Password"
          value={form.values.password}
          error={form.errors.password}
          onChange={onChange}
        />
        <Space h="lg" />
        <Button type="submit" loading={submit === "pending"}>Sign up</Button>
        <Space h="xl" />
        <Link to="/signin">Sign in</Link>
      </form>
    </Center>)
}

export default Signup;