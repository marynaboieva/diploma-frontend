import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";

import { getUserRequest } from '../redux/slices/user/actions';
import { getChatsRequest } from '../redux/slices/chats/actions';

import Home from './Home';
import Signin from './Signin';
import Signup from './Signup';

import PrivateRoute from './PrivateRoute';
import AuthLayout from '../containers/AuthLayout';
import AppLayout from '../containers/AppLayout';
import { selectUserState } from '../redux/slices/user/selectors';


function Pages() {
  const user = useSelector(selectUserState);

  const dispatch = useDispatch();

  useEffect(() => {
    if (user.hasToken && ["idle", "rejected"].includes(user.loading)) {
      dispatch(getUserRequest());
      dispatch(getChatsRequest());
    }
  }, [dispatch, user]);

  return (
    <BrowserRouter>
      <Routes>
        <Route element={<PrivateRoute />} >
          <Route path="/" element={<AppLayout><Home /></AppLayout>} />
        </Route>
        <Route path="/signin" element={<AuthLayout> <Signin /></AuthLayout>} />
        <Route path="/signup" element={<AuthLayout><Signup /></AuthLayout>} />
        <Route
          path="*"
          element={<Navigate to="/" />}
        />
      </Routes>
    </BrowserRouter>
  );
}

export default Pages;
