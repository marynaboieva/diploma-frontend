import React, { FC, useCallback, } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { TextInput, PasswordInput, Button, Center, Space } from '@mantine/core';
import { useForm } from '@mantine/hooks';

import { loginRequest } from '../redux/slices/user/actions'
import { selectUserState } from '../redux/slices/user/selectors'

import { LoginRequestData } from '../services/types';
import { Link } from 'react-router-dom';

const Signin: FC = () => {
  const form = useForm<LoginRequestData>({
    initialValues: {
      email: '',
      password: '',
    },
    validationRules: {
      email: (value) => /^\S+@\S+$/.test(value),
      password: (value) => !!value
    },
  });

  const { submit } = useSelector(selectUserState);

  const dispatch = useDispatch();

  const onChange = useCallback((event) => {
    form.setFieldValue(event.target.name, event.target.value);
  }, [form])

  const onSubmit = useCallback((values) => {
    dispatch(loginRequest(values));
  }, [dispatch])

  return (
    <Center style={{ height: '100vh' }}>
      <form onSubmit={form.onSubmit(onSubmit)}>
        <TextInput
          required
          name="email"
          label="Email"
          placeholder="your@email.com"
          value={form.values.email}
          error={form.errors.email}
          onChange={onChange}
        />
        <PasswordInput
          required
          name="password"
          label="Password"
          value={form.values.password}
          error={form.errors.password}
          onChange={onChange}
        />
        <Space h="lg" />
        <Button type="submit" loading={submit === "pending"}>Sign in</Button>
        <Space h="xl" />
        <Link to="/signup">Sign up</Link>
      </form>
    </Center>)
}

export default Signin;