import { FC } from "react";
import { useSelector } from "react-redux";
import { Navigate, Outlet } from "react-router-dom";
import { selectUserState } from "../redux/slices/user/selectors";

const PrivateRoute: FC = () => {
  const { hasToken } = useSelector(selectUserState);

  return hasToken ? <Outlet /> : <Navigate to="/signin" />;
};

export default PrivateRoute;
