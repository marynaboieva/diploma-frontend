import { miniSerializeError } from "@reduxjs/toolkit";
import axios from "axios";
import { getToken } from "./TokenService";


const baseURL = process.env.REACT_APP_BASE_URL;

const axiosClient = axios.create({
  baseURL,
});

axiosClient.interceptors.request.use(
  async (config) => {
    config.headers = {
      Authorization: `Bearer ${getToken()}`,
      "Content-type": "application/json",
    };
    return config;
  },
  (error) => {
    Promise.reject(miniSerializeError(error));
  }
);

axios.interceptors.response.use(
  (response) => response,
  (error) => Promise.reject(miniSerializeError(error))
);

export default axiosClient;