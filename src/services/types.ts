export type RequestStatus = "idle" | "pending" | "fulfilled" | "rejected";
export interface LoginRequestData {
  email: string;
  password: string;
}

export interface RegisterRequestData {
  email: string;
  password: string;
  username: string;
}

export interface User {
  email: string;
  username: string;
  id: number;
}

export interface Chat {
  id: number;
  name: string;
  members: User[]
}
export interface CreateChatData {
  name: string;
}