import axiosClient from "./AxiosClient";
import { Chat, CreateChatData } from "./types";

export const getChats = async (): Promise<Chat[]> => {
  const result = await axiosClient.get('/chats')
  return result.data;
};

export const createChat = async (payload: CreateChatData): Promise<Chat> => {
  const result = await axiosClient.post('/chats', payload)
  return result.data;
};

