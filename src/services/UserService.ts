import axiosClient from "./AxiosClient";
import { removeToken, setToken } from "./TokenService";
import { LoginRequestData, RegisterRequestData, User } from "./types";

export const register = async (registerData: RegisterRequestData): Promise<User> => {
  const result = await axiosClient.post('/users', registerData);

  return result.data.User;
};

export const login = async (
  { email, password }: LoginRequestData,
): Promise<string> => {

  const result = await axiosClient.post(
    '/auth/login',
    { email, password },
  );

  if (result.data.access_token) {
    setToken(result.data.access_token);
  }

  return result.data.access_token;
};

export const logout = () => {
  removeToken();
};

export const getUser = async (): Promise<User> => {
  const result = await axiosClient.get(`/users/`);

  return result.data;
};