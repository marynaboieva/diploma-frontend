import React from 'react';
import { MantineProvider } from '@mantine/core';
import { Provider } from 'react-redux'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import store from './redux/store';
import Pages from './pages/Pages';


function App() {
  return (
    <MantineProvider>
      <Provider store={store}>
        <Pages />
        <ToastContainer />
      </Provider>
    </MantineProvider>
  );
}

export default App;
