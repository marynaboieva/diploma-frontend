import React, { FC, useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { selectUserState } from "../redux/slices/user/selectors";

const AuthLayout: FC = ({ children }) => {
  const { hasToken, submit } = useSelector(selectUserState);

  const navigate = useNavigate();

  useEffect(() => {
    if (hasToken && submit === "fulfilled") {
      navigate("/");
    }
  }, [navigate, hasToken, submit])

  return <>{children}</>
};

export default AuthLayout;
