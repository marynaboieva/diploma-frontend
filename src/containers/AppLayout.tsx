import React, { FC } from "react";
import { useSelector } from "react-redux";
import { AppShell } from "@mantine/core";
import { useBooleanToggle } from "@mantine/hooks";
import Navbar from "../components/Navbar";
import Header from "../components/Header";

import { selectUserState } from "../redux/slices/user/selectors";
import { Navigate } from "react-router";

const AppLayout: FC = ({ children }) => {
  const [open, toggle] = useBooleanToggle(false);

  const { hasToken } = useSelector(selectUserState);

  return hasToken ? <AppShell
    navbarOffsetBreakpoint="md"
    fixed
    navbar={<Navbar open={open} />}
    header={<Header open={open} toggleOpen={toggle} height={70} />}

  >
    {children}
  </AppShell> : <Navigate to="/signup" />
};

export default AppLayout;
