import { AppState } from "../../store";


export const selectUserState = (state: AppState) => state.user;
