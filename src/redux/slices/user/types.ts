import { RequestStatus } from "../../../services/types";
import { User } from "../../../services/types";

export interface UserSate {
  loading: RequestStatus,
  submit: RequestStatus,
  hasToken: boolean,
  userData: User
}