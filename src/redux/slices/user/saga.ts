import { all, call, put, takeLatest } from 'redux-saga/effects'

import {
  loginRequest,
  loginSuccess,
  loginFail,
  registerRequest,
  registerSuccess,
  registerFail,
  logoutSuccess,
  getUserSuccess,
  getUserFail,
  logoutFail,
  getUserRequest,
  logoutRequest
} from './actions'

import * as UserSevice from '../../../services/UserService';
import { User } from '../../../services/types';

export function* getUser() {
  try {
    const userData: User = yield call(UserSevice.getUser);
    yield put(getUserSuccess(userData));
  }
  catch (error) {
    yield put(getUserFail(error));
  }
}

export function* login({ payload }: ReturnType<typeof loginRequest>) {
  try {
    yield call(UserSevice.login, payload);

    yield put(loginSuccess());
  }
  catch (error) {
    yield put(loginFail(error));
  }
}

export function* logout() {
  try {
    yield call(UserSevice.logout);
    yield put(logoutSuccess());
  }
  catch (error) {
    yield put(logoutFail(error));
  }
}

export function* register({ payload }: ReturnType<typeof registerRequest>) {
  try {
    const userData: User = yield call(UserSevice.register, payload);

    yield put(registerSuccess(userData));
    yield put(loginRequest(payload));
  }
  catch (error) {
    yield put(registerFail(error));
  }
}

const userDataSaga = all([
  takeLatest(String(getUserRequest), getUser),
  takeLatest(String(loginRequest), login),
  takeLatest(String(registerRequest), register),
  takeLatest(String(logoutRequest), logout),
]);

export default userDataSaga