import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { UserSate } from "./types";
import {
  getUserFail,
  getUserRequest,
  getUserSuccess,
  loginFail,
  loginRequest,
  loginSuccess,
  logoutSuccess,
  registerFail,
  registerRequest,
  registerSuccess
} from "./actions";
import { getToken } from "../../../services/TokenService";
import { RequestStatus, User } from "../../../services/types";


const initialState: UserSate = {
  userData: {
    username: "",
    email: "",
    id: 0,
  },
  loading: "idle",
  submit: "idle",
  hasToken: !!getToken()

};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {},
  extraReducers: {
    [String(loginRequest)]: (state: UserSate) => ({ ...state, submit: "pending" as RequestStatus }),
    [String(loginFail)]: (state: UserSate) => ({ ...state, hasToken: false, submit: "rejected" as RequestStatus }),
    [String(loginSuccess)]: (state: UserSate) => ({ ...state, hasToken: true, submit: "fulfilled" as RequestStatus }),

    [String(registerRequest)]: (state: UserSate) => ({ ...state, submit: "pending" as RequestStatus }),
    [String(registerFail)]: (state: UserSate) => ({ ...state, submit: "rejected" as RequestStatus }),
    [String(registerSuccess)]: (state: UserSate) => ({ ...state, submit: "fulfilled" as RequestStatus }),

    [String(logoutSuccess)]: () => initialState,

    [String(getUserRequest)]: (state: UserSate) => ({ ...state, loading: "pending" as RequestStatus }),
    [String(getUserFail)]: (state: UserSate) => ({ ...state, loading: "rejected" as RequestStatus, hasToken: false }),
    [String(getUserSuccess)]: (state: UserSate, { payload }: PayloadAction<User>) => ({
      ...state,
      loading: "fulfilled" as RequestStatus,
      userData: payload
    }),
  },
});

export default userSlice.reducer;
