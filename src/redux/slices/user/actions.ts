import { createAction, } from '@reduxjs/toolkit'
import { User } from '../../../services/types';
import { LoginRequestData, RegisterRequestData } from '../../../services/types';

export const loginRequest = createAction<LoginRequestData>("user/loginRequest");
export const loginFail = createAction<any>("user/loginFail");
export const loginSuccess = createAction("user/loginSuccess");

export const registerRequest = createAction<RegisterRequestData>("user/registerRequest");
export const registerFail = createAction<any>("user/registerFail");
export const registerSuccess = createAction<User>("user/registerSuccess");

export const logoutRequest = createAction("user/logoutRequest");
export const logoutSuccess = createAction("user/logoutSuccess");
export const logoutFail = createAction<any>("user/logoutFail");

export const getUserRequest = createAction("user/getUserRequest");
export const getUserFail = createAction<any>("user/getUserFail");
export const getUserSuccess = createAction<User>("user/getUserSuccess");