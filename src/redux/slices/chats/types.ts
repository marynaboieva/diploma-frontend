import { RequestStatus } from "../../../services/types";

export interface ChatsSate {
  loading: RequestStatus,
  create: RequestStatus,
}