import store, { AppState } from "../../store";
import { chatsAdapter } from "./chatsSlice";


const chatsSelectors = chatsAdapter.getSelectors<AppState>(
  (state) => state.chats
)

export const getAllChats = chatsSelectors.selectAll(store.getState())
export const getChatById = ((id: number) => chatsSelectors.selectById(store.getState(), id));