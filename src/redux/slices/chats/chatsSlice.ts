import { createEntityAdapter, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ChatsSate } from "./types";
import {
  createChatFail,
  createChatRequest,
  createChatSuccess,
  getChatsFail,
  getChatsRequest,
  getChatsSuccess
} from "./actions";
import { Chat } from "../../../services/types";


const initialState: ChatsSate = {
  loading: "idle",
  create: "idle"
};

export const chatsAdapter = createEntityAdapter<Chat>({
  selectId: (chat) => chat.id,
  sortComparer: (a, b) => a.name.localeCompare(b.name),
})

const chatsSlice = createSlice({
  name: "chats",
  initialState: chatsAdapter.getInitialState(initialState),
  reducers: {},
  extraReducers: {
    [String(getChatsFail)]: (state) => {
      state.loading = "pending"
    },
    [String(getChatsRequest)]: (state) => {
      state.loading = "rejected"
    },
    [String(getChatsSuccess)]: (state, { payload }: PayloadAction<Chat[]>) => {
      state.loading = "fulfilled";
      chatsAdapter.setAll(state, payload);
    },

    [String(createChatRequest)]: (state) => {
      state.create = "pending"
    },
    [String(createChatFail)]: (state) => {
      state.create = "rejected"
    },
    [String(createChatSuccess)]: (state, { payload }: PayloadAction<Chat>) => {
      state.create = "fulfilled";
      chatsAdapter.addOne(state, payload);
    },
  }
});

export default chatsSlice.reducer;
