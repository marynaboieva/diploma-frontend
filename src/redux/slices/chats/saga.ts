import { all, call, put, takeLatest } from 'redux-saga/effects'

import * as ChatsService from '../../../services/ChatsService';
import { Chat } from '../../../services/types';
import { getChatsFail, getChatsSuccess, getChatsRequest } from './actions';

export function* getChats() {
  try {
    const catsList: Chat[] = yield call(ChatsService.getChats);
    yield put(getChatsSuccess(catsList));
  }
  catch (error) {
    yield put(getChatsFail(error));
  }
}

const chatsSaga = all([
  takeLatest(String(getChatsRequest), getChats),

]);

export default chatsSaga