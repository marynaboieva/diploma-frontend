import { createAction, } from '@reduxjs/toolkit'
import { Chat, CreateChatData } from '../../../services/types';

export const getChatsRequest = createAction("user/getChatsRequest");
export const getChatsFail = createAction<any>("user/getChatsFail");
export const getChatsSuccess = createAction<Chat[]>("user/getChatsSuccess");

export const createChatRequest = createAction<CreateChatData>("user/createChatRequest");
export const createChatFail = createAction<any>("user/createChatFail");
export const createChatSuccess = createAction<Chat>("user/createChatSuccess")
