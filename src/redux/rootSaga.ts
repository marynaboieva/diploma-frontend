import { all } from 'redux-saga/effects';

import userSaga from './slices/user/saga';
import chatsSaga from './slices/chats/saga';

import errorsSaga from './errorsSaga';

export default function* rootSaga() {
  yield all([errorsSaga, userSaga, chatsSaga]);
}