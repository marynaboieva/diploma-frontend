import { combineReducers } from "@reduxjs/toolkit";

import userReducer from './slices/user/userSlice'
import chatsReducer from './slices/chats/chatsSlice'

const rootReducer = combineReducers({
  user: userReducer,
  chats: chatsReducer
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;