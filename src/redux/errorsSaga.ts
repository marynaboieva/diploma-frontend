import { takeEvery } from "@redux-saga/core/effects";
import { AxiosError } from "axios";
import { toast } from "react-toastify";
import { loginFail, registerFail, getUserFail, logoutFail, } from "./slices/user/actions";
import { getChatsFail, createChatFail } from "./slices/chats/actions";

// eslint-disable-next-line require-yield
function* notify({ payload: error, type }: { payload: AxiosError, type: string }) {
  let message = error.message === 'Network Error'
    ? error.message
    : 'Something went wrong, please try again later.';

  const errorData = error?.response?.data;
  toast.error(errorData.message ?? message);
}

const errorsSaga = takeEvery([
  String(loginFail),
  String(registerFail),
  String(getUserFail),
  String(logoutFail),
  String(getChatsFail),
  String(createChatFail)
], notify);

export default errorsSaga;