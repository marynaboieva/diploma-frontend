import React, { FC } from "react";
import { Burger, Header as HeaderComp, HeaderProps, MediaQuery, useMantineTheme } from "@mantine/core";

type Props = Omit<HeaderProps, 'children'> & { open: boolean, toggleOpen: () => void };

const Header: FC<Props> = ({ open, toggleOpen, ...props }) => {
  const theme = useMantineTheme();

  return <HeaderComp {...props} padding="md">
    <div style={{ display: 'flex', alignItems: 'center', height: '100%' }}>
      <MediaQuery smallerThan="md" styles={{ display: 'none' }}>
        <Burger
          opened={open}
          onClick={() => toggleOpen()}
          size="sm"
          color={theme.colors.gray[6]}
          mr="xl"
        />
      </MediaQuery>
      title
    </div>
  </HeaderComp>
};

export default Header;
