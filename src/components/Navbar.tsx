import React, { FC } from "react";
import { Navbar as NavbarComp, NavbarProps } from "@mantine/core";

type Props = Omit<NavbarProps, 'children'> & { open: boolean };

const Navbar: FC<Props> = ({ open, ...props }) => {

  return <NavbarComp
    {...props}
    padding="md"
    hiddenBreakpoint="md"
    hidden={!open}
    width={{ md: 300, lg: 400 }}
  >
    menu
  </NavbarComp>
};

export default Navbar;
